from pathlib import Path

from setuptools import find_packages, setup


def get_requirements(path):
    lines = Path(path).open('r')
    lines = (l.strip() for l in lines)
    lines = (l for l in lines if bool(l))
    lines = (l for l in lines if not l.startswith('#'))

    return list(lines)


README = Path('README.md').open('r').read()
REQUIREMENTS = get_requirements('requirements.txt')


setup(
    name='omekatopkg',
    description='Make content packages out of media files',
    long_description=README,
    version='0.0',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'Intended Audience :: Developers',
        ('License :: OSI Approved :: GNU Affero General Public License v3 or '
         'later (AGPLv3+)'),
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3 :: Only',
    ],
    url='https://framagit.org/ideascube/omeka-to-pkg/',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=REQUIREMENTS,
    entry_points={
        'console_scripts': [
            'omeka-to-pkg = omekatopkg:main',
        ],
    },
)
