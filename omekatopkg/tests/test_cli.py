import os
import zipfile

import pytest

import yaml


def test_omeka_to_pkg_command_with_unsupported_format(
        runner, file_writer, package_path, capsys):
    xml_path = file_writer('whatever metadata', 'xml')

    with pytest.raises(SystemExit):
        runner(xml_path, package_path)

    out, err = capsys.readouterr()

    assert out.strip() == ''
    assert err.strip() == '\n'.join([
        'Input file must be in one of the following formats:',
        ' * csv',
        ' * json',
    ])


def test_omeka_to_pkg_command(runner, file_writer, package_path, capsys):
    metadata = ('kind,title,summary,credits,path\n'
                'video,my video,my video summary,BSF,a-video.mp4\n'
                'pdf,my doc,my doc summary,BSF,a-pdf.pdf\n'
                'image,my image,my image summary,BSF,an-image.jpg\n')
    csv_path = file_writer(metadata, 'csv')
    runner(csv_path, package_path)
    out, err = capsys.readouterr()

    with zipfile.ZipFile(package_path) as package:
        assert set(package.namelist()) == set(['manifest.yml',
                                               'a-video.mp4',
                                               'a-pdf.pdf',
                                               'an-image.jpg'])
        with package.open('manifest.yml') as m:
            manifest = yaml.load(m)
        assert manifest == {
            'medias': [
                {
                    'title': 'my video',
                    'summary': 'my video summary',
                    'kind': 'video',
                    'credits': 'BSF',
                    'path': 'a-video.mp4',
                },
                {
                    'title': 'my doc',
                    'summary': 'my doc summary',
                    'kind': 'pdf',
                    'credits': 'BSF',
                    'path': 'a-pdf.pdf',
                },
                {
                    'title': 'my image',
                    'summary': 'my image summary',
                    'kind': 'image',
                    'credits': 'BSF',
                    'path': 'an-image.jpg',
                }
            ]
        }

    msg = "Package {} has been created with 3 medias.".format(package_path)
    assert msg in out
    # Check the order of the output
    index = 0
    for k in (
            'name', 'description', 'language', 'version', 'url', 'size',
            'sha256sum', 'type'):
        index = out.find('    '+k, index)
        assert index != -1, '{} is not in the right order'.format(k)


def test_omeka_to_pkg_wrong_metadata_command(
        runner, file_writer, package_path, capsys):
    metadata = ('kind,title,summary,credits,path\n'
                'video,my video,my video summary,BSF,a-video.mp4\n'
                'pdf,my doc,my doc summary,BSF,a-pdf.pdf\n'
                'image,,my image summary,BSF,an-image.jpg\n'
                'image,my image,,BSF,an-image.jpg\n'
                'image,my image,my image summary,,an-image.jpg\n'
                'image,my image,my image summary,BSF,\n'
                'image,my image,my image summary,BSF,an-image2.jpg\n')
    csv_path = file_writer(metadata, 'csv')
    runner(csv_path, package_path)
    out, err = capsys.readouterr()

    with zipfile.ZipFile(package_path) as package:
        assert set(package.namelist()) == set(['manifest.yml',
                                               'a-video.mp4',
                                               'a-pdf.pdf'])
        with package.open('manifest.yml') as m:
            manifest = yaml.load(m)
        assert manifest == {
            'medias': [
                {
                    'title': 'my video',
                    'summary': 'my video summary',
                    'kind': 'video',
                    'credits': 'BSF',
                    'path': 'a-video.mp4',
                },
                {
                    'title': 'my doc',
                    'summary': 'my doc summary',
                    'kind': 'pdf',
                    'credits': 'BSF',
                    'path': 'a-pdf.pdf',
                }
            ]
        }

    msg = "Package {} has been created with 2 medias.".format(package_path)
    assert msg in out


def test_omeka_to_pkg_wrong_metadata_verbose_command(
        runner, file_writer, package_path, capsys):
    metadata = ('kind,title,summary,credits,path\n'
                'video,my video,my video summary,BSF,a-video.mp4\n'
                'pdf,my doc,my doc summary,BSF,a-pdf.pdf\n'
                'image,,my image summary,BSF,an-image.jpg\n'
                'image,my image,,BSF,an-image.jpg\n'
                'image,my image,my image summary,,an-image.jpg\n'
                'image,my image,my image summary,BSF,\n'
                'image,my image,my image summary,BSF,an-image2.jpg\n')
    csv_path = file_writer(metadata, 'csv')
    runner(csv_path, package_path)
    out, err = capsys.readouterr()

    with zipfile.ZipFile(package_path) as package:
        assert set(package.namelist()) == set(['manifest.yml',
                                               'a-video.mp4',
                                               'a-pdf.pdf'])
        with package.open('manifest.yml') as m:
            manifest = yaml.load(m)
        assert manifest == {
            'medias': [
                {
                    'title': 'my video',
                    'summary': 'my video summary',
                    'kind': 'video',
                    'credits': 'BSF',
                    'path': 'a-video.mp4',
                },
                {
                    'title': 'my doc',
                    'summary': 'my doc summary',
                    'kind': 'pdf',
                    'credits': 'BSF',
                    'path': 'a-pdf.pdf',
                }
            ]
        }

    assert "title missing, ignoring media 3" in err
    assert "summary missing, ignoring media 4" in err
    assert "credits missing, ignoring media 5" in err
    assert "path missing, ignoring media 6" in err
    assert "ignoring media 7: " in err
    assert "data/an-image2.jpg" in err

    msg = "Package {} has been created with 2 medias.".format(package_path)
    assert msg in out


def test_omeka_to_pkg_no_valid_media_verbose_command(
        runner, file_writer, package_path, capsys):
    metadata = ('kind,title,summary,credits,path\n'
                'image,,my image summary,BSF,an-image.jpg\n')
    csv_path = file_writer(metadata, 'csv')
    with pytest.raises(SystemExit):
        runner(csv_path, package_path)
    out, err = capsys.readouterr()

    assert "There is no (valid) media to create the package." in err
    assert "title missing, ignoring media 1" in err
    assert not os.path.exists(package_path)


def test_omeka_to_pkg_dry_run_do_not_create_package(
        runner, file_writer, package_path, capsys):
    metadata = ('kind,title,summary,credits,path\n'
                'video,my video,my video summary,BSF,a-video.mp4\n'
                'pdf,my doc,my doc summary,BSF,a-pdf.pdf\n'
                'image,my image,my image summary,BSF,an-image.jpg\n')
    csv_path = file_writer(metadata, 'csv')
    runner('--dry-run', csv_path, package_path)
    out, err = capsys.readouterr()
    assert "Package {} would have been created with 3 medias.".format(
        package_path) in out
    assert not os.path.exists(package_path)


def test_fail_cleanly_if_input_file_does_not_exist(runner, tmpdir, capsys):
    csv_path = tmpdir.join('a-file-which-does-not-exist.csv').strpath
    package_path = tmpdir.join('the-output-package').strpath

    with pytest.raises(SystemExit):
        runner(csv_path, package_path)

    out, err = capsys.readouterr()
    assert err.strip() == 'Path does not exist: {}'.format(csv_path)
    assert not os.path.exists(package_path)
