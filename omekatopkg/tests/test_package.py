import json
import os
import zipfile

import pytest

import yaml


def test_create_a_package_from_csv(file_writer):
    from omekatopkg.package import MediaCenterPackage

    metadata = ('kind,title,summary,credits,path\n'
                'video,my video,my video summary,BSF,a-video.mp4\n'
                'pdf,my doc,my doc summary,BSF,a-pdf.pdf\n'
                'image,my image,my image summary,BSF,an-image.jpg\n')
    csv_path = file_writer(metadata, 'csv')
    package = MediaCenterPackage.from_csv(csv_path)
    assert len(package.medias) == 3
    assert package.medias == [
        {
            'title': 'my video',
            'summary': 'my video summary',
            'kind': 'video',
            'credits': 'BSF',
            'path': 'a-video.mp4',
        },
        {
            'title': 'my doc',
            'summary': 'my doc summary',
            'kind': 'pdf',
            'credits': 'BSF',
            'path': 'a-pdf.pdf',
        },
        {
            'title': 'my image',
            'summary': 'my image summary',
            'kind': 'image',
            'credits': 'BSF',
            'path': 'an-image.jpg',
        }
    ]


@pytest.mark.parametrize('metadata', [
    ('kind,title,summary,credits,path,preview\n'
     'video,my video,my video summary,BSF,a-video.mp4,'),
    ('kind,title,summary,credits,path,preview\n'
     'video,my video,my video summary,BSF,a-video.mp4,NOT_EXIST'),
    ('kind,title,summary,credits,path\n'
     'video,my video,my video summary,BSF,a-video.mp4')
])
def test_no_preview_in_csv(file_writer, metadata):
    from omekatopkg.package import MediaCenterPackage

    csv_path = file_writer(metadata, 'csv')
    package = MediaCenterPackage.from_csv(csv_path)
    assert not package.medias[0].get('preview')


def test_preview_in_csv(file_writer):
    from omekatopkg.package import MediaCenterPackage

    metadata = ('kind,title,summary,credits,path,preview\n'
                'video,my video,my video summary,BSF,a-video.mp4,an-image.jpg')
    csv_path = file_writer(metadata, 'csv')
    package = MediaCenterPackage.from_csv(csv_path)
    assert package.medias[0]['preview'] == 'an-image.jpg'


@pytest.mark.parametrize('row', [
    'image,,my summary,BSF,an-image.jpg',
    'image,my title,,BSF,an-image.jpg',
    'image,my title,my summary,,an-image.jpg',
    'image,my title,my summary,BSF,',
    'image,my title,my summary,BSF,unknownpath.mp4',
])
def test_should_skip_if_missing_required_metadata(
        package_path, file_writer, row):
    from omekatopkg.package import MediaCenterPackage

    metadata = ('kind,title,summary,credits,path\n' + row)
    csv_path = file_writer(metadata, 'csv')
    package = MediaCenterPackage.from_csv(csv_path)
    assert not package.medias


def test_create_zip_package(package_path):
    from omekatopkg.package import MediaCenterPackage

    package = MediaCenterPackage(
        os.path.join(os.path.dirname(__file__), 'data'),
        medias=[
            {
                'title': 'my video',
                'summary': 'my video summary',
                'kind': 'video',
                'credits': 'BSF',
                'path': 'a-video.mp4',
                'preview': 'an-image1.jpg',
            },
            {
                'title': 'my doc',
                'summary': 'my doc summary',
                'kind': 'pdf',
                'credits': 'BSF',
                'path': 'a-pdf.pdf',
                'preview': 'an-image.jpg',
            },
            {
                'title': 'my image',
                'summary': 'my image summary',
                'kind': 'image',
                'credits': 'BSF',
                'path': 'an-image.jpg',
            }])
    package.create_package_zip(package_path)
    with zipfile.ZipFile(package_path) as package:
        assert set(package.namelist()) == set(['manifest.yml',
                                               'a-video.mp4',
                                               'a-pdf.pdf',
                                               'an-image.jpg',
                                               'an-image1.jpg'])
        with package.open('manifest.yml') as m:
            manifest = yaml.load(m)
        assert manifest == {
            'medias': [
                {
                    'title': 'my video',
                    'summary': 'my video summary',
                    'kind': 'video',
                    'credits': 'BSF',
                    'path': 'a-video.mp4',
                    'preview': 'an-image1.jpg',
                },
                {
                    'title': 'my doc',
                    'summary': 'my doc summary',
                    'kind': 'pdf',
                    'credits': 'BSF',
                    'path': 'a-pdf.pdf',
                    'preview': 'an-image.jpg',
                },
                {
                    'title': 'my image',
                    'summary': 'my image summary',
                    'kind': 'image',
                    'credits': 'BSF',
                    'path': 'an-image.jpg',
                }
            ]
        }


def test_create_a_package_from_json(file_writer):
    from omekatopkg.package import MediaCenterPackage

    metadata = json.dumps([
        {
            'kind': 'video', 'title': 'my video', 'credits': 'BSF',
            'summary': 'my video summary', 'path': 'a-video.mp4',
        },
        {
            'kind': 'pdf', 'title': 'my doc', 'credits': 'BSF',
            'summary': 'my doc summary', 'path': 'a-pdf.pdf',
        },
        {
            'kind': 'image', 'title': 'my image', 'credits': 'BSF',
            'summary': 'my image summary', 'path': 'an-image.jpg',
        },
    ])
    json_path = file_writer(metadata, 'json')
    package = MediaCenterPackage.from_json(json_path)
    assert len(package.medias) == 3
    assert package.medias == [
        {
            'title': 'my video',
            'summary': 'my video summary',
            'kind': 'video',
            'credits': 'BSF',
            'path': 'a-video.mp4',
        },
        {
            'title': 'my doc',
            'summary': 'my doc summary',
            'kind': 'pdf',
            'credits': 'BSF',
            'path': 'a-pdf.pdf',
        },
        {
            'title': 'my image',
            'summary': 'my image summary',
            'kind': 'image',
            'credits': 'BSF',
            'path': 'an-image.jpg',
        }
    ]


@pytest.mark.parametrize('metadata', [
    [
        {
            'kind': 'video', 'title': 'my video', 'credits': 'BSF',
            'summary': 'my video summary', 'path': 'a-video.mp4',
            'preview': '',
        },
    ],
    [
        {
            'kind': 'pdf', 'title': 'my doc', 'credits': 'BSF',
            'summary': 'my doc summary', 'path': 'a-pdf.pdf',
            'preview': 'DOES_NOT_EXIST',
        },
    ],
    [
        {
            'kind': 'image', 'title': 'my image', 'credits': 'BSF',
            'summary': 'my image summary', 'path': 'an-image.jpg',
        },
    ],
])
def test_no_preview_in_json(file_writer, metadata):
    from omekatopkg.package import MediaCenterPackage

    json_path = file_writer(json.dumps(metadata), 'json')
    package = MediaCenterPackage.from_json(json_path)
    assert len(package.medias) == 1
    assert not package.medias[0].get('preview')


def test_preview_in_json(file_writer):
    from omekatopkg.package import MediaCenterPackage

    metadata = json.dumps([
        {
            'kind': 'video', 'title': 'my video', 'credits': 'BSF',
            'summary': 'my video summary', 'path': 'a-video.mp4',
            'preview': 'an-image.jpg',
        },
    ])
    json_path = file_writer(metadata, 'json')
    package = MediaCenterPackage.from_json(json_path)
    assert package.medias[0]['preview'] == 'an-image.jpg'


@pytest.mark.parametrize('metadata', [
    [
        {
            'kind': 'image', 'credits': 'BSF',
            'summary': 'my summary', 'path': 'an-image.jpg',
        },
    ],
    [
        {
            'kind': 'image', 'title': 'my title', 'credits': 'BSF',
            'path': 'an-image.jpg',
        },
    ],
    [
        {
            'kind': 'image', 'title': 'my title',
            'summary': 'my summary', 'path': 'an-image.jpg',
        },
    ],
    [
        {
            'kind': 'image', 'title': 'my title', 'credits': 'BSF',
            'summary': 'my summary',
        },
    ],
    [
        {
            'kind': 'image', 'title': 'my title', 'credits': 'BSF',
            'summary': 'my summary', 'path': 'unknownpath.jpg',
        },
    ],
])
def test_should_skip_if_missing_required_metadata_in_json(
        package_path, file_writer, metadata):
    from omekatopkg.package import MediaCenterPackage

    json_path = file_writer(json.dumps(metadata), 'json')
    package = MediaCenterPackage.from_json(json_path)
    assert not package.medias
