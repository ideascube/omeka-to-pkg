import pytest


@pytest.fixture
def testdatadir(request):
    datadir = request.fspath.dirpath().join('data')
    assert datadir.check(dir=True)

    return datadir


@pytest.fixture
def file_writer(request, testdatadir):
    def clean(path):
        if path.check(file=1):
            path.remove()

    def write(metadata, extension):
        path = testdatadir.join('thefile.%s' % extension)
        path.write_text(metadata, encoding='utf-8')

        request.addfinalizer(lambda: clean(path))

        return path.strpath

    return write


@pytest.fixture
def package_path(tmpdir):
    return tmpdir.join("test_package.zip").strpath


@pytest.fixture
def runner():
    def run(*args):
        from omekatopkg import get_parser, handle

        args = get_parser().parse_args(args)
        return handle(args)

    return run
