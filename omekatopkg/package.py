import csv
import json
import logging
import os.path as op
import zipfile

import yaml


class MediaCenterPackage:
    def __init__(self, working_dir, medias=None):
        self.working_dir = working_dir
        self.medias = []
        self.logger = logging.getLogger(__name__)
        for index, media in enumerate(medias or [], 1):
            if self.check_media(index, media):
                self.medias.append(media)

    def check_media(self, index, media):
        for required_metadata in ('title', 'summary', 'credits', 'path'):
            if not media.get(required_metadata):
                self.logger.warning("{} missing, ignoring media {}".format(
                    required_metadata, index))
                return False
        full_path = op.join(self.working_dir, media['path'])
        if not op.exists(full_path):
            self.logger.warning(
                "Path does not exist, ignoring media {}: {}".format(
                    index, full_path))
            return False
        preview_path = media.get('preview')
        if (preview_path and
                not op.exists(op.join(self.working_dir, preview_path))):
            self.logger.warning(
                "Preview path does not exist for media {}: {}".format(
                    index, preview_path))
            media['preview'] = None
        return True

    @classmethod
    def from_csv(cls, csv_path):
        working_dir = op.dirname(csv_path)
        with open(csv_path, 'r', encoding='utf-8') as f:
            content = f.read()
            try:
                dialect = csv.Sniffer().sniff(content)
            except csv.Error:
                dialect = csv.unix_dialect()
            reader = csv.DictReader(content.splitlines(), dialect=dialect)
            medias = list(reader)
            return cls(working_dir, medias)

    @classmethod
    def from_json(cls, json_path):
        working_dir = op.dirname(json_path)

        with open(json_path, 'r', encoding='utf-8') as f:
            medias = json.load(f)
            return cls(working_dir, medias)

    def create_package_zip(self, archive_path):
        with zipfile.ZipFile(archive_path,
                             mode='w',
                             allowZip64=True) as ziparchive:
            for mediaInfo in self.medias:
                filename = op.join(self.working_dir, mediaInfo['path'])
                ziparchive.write(filename=filename, arcname=mediaInfo['path'])
                preview_path = mediaInfo.get('preview')
                if preview_path:
                    preview_path = op.join(self.working_dir, preview_path)
                    ziparchive.write(filename=preview_path,
                                     arcname=mediaInfo['preview'])
            ziparchive.writestr('manifest.yml', self.dump_yaml())

    def dump_yaml(self):
        dump = {'medias': self.medias}
        return yaml.dump(dump, default_flow_style=None)
