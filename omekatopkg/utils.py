import hashlib
import os


def get_file_sha256(path):
    sha = hashlib.sha256()

    with open(path, 'rb') as f:
        while True:
            data = f.read(8388608)

            if not data:
                break

            sha.update(data)

    return sha.hexdigest()


def get_file_size(path):
    return os.stat(path).st_size
