import argparse
import os
import sys
from datetime import date
from collections import OrderedDict

import yaml

from .package import MediaCenterPackage
from .utils import get_file_sha256, get_file_size


help_str = """Convert a file describing a set of media into a package.

The file must contain "title", "summary", "path" and "credits" properties for
each media.
Optional properties are "lang", "preview", "kind", "tags".
Attributes "path" and "preview" (if provided) must be paths
(relative to the directory containing the input file) to existing files.

omeka-to-pkg also generates a yaml to add to the catalog.
Some attributes are automically calculated from the generated package.
Other attributes may be provided as command arguments.
"""

SUPPORTED_FORMATS = (
    'csv',
    'json',
)


def ordered_representer(dumper, data):
    return dumper.represent_mapping('tag:yaml.org,2002:map', data.items())

yaml.add_representer(OrderedDict, ordered_representer, Dumper=yaml.SafeDumper)


def abort(msg):
    if not msg.endswith('\n'):
        msg += '\n'

    sys.stderr.write(msg)
    sys.stderr.flush()
    sys.exit(1)


def generate_catalog_yaml_metadata(path, metadata, _type='zipped-medias'):
    return {
        'all': {
            metadata['package_id']: OrderedDict([
                ('name', metadata['name']),
                ('description', metadata['description']),
                ('language', metadata['language']),
                ('version', date.today().isoformat()),
                ('url', metadata['url']),
                ('size', get_file_size(path)),
                ('sha256sum', get_file_sha256(path)),
                ('type', _type)
            ])
        }
    }


def get_parser():
    parser = argparse.ArgumentParser(description=help_str)

    parser.add_argument('metadata_path', help='Path to input file.')
    parser.add_argument('package_path', help='Path of the package to create')
    parser.add_argument('--dry-run', action='store_true',
                        help='Do not really create the package.')

    group = parser.add_argument_group('yaml generation arguments')
    group.add_argument('--package-id', default="<package_id>",
                       help="Identifier of the package.")
    group.add_argument('--name', default="<Package name>",
                       help="Name of the package.")
    group.add_argument('--description', default="<Package description>",
                       help="Description of the package.")
    group.add_argument('--language', default="<lang>",
                       help="Language of the package.")
    group.add_argument('--url', default="<http://...>",
                       help="URL where the package will be available")

    return parser


def handle(args):
    metadata_path = os.path.abspath(args.metadata_path)
    package_path = os.path.abspath(args.package_path)
    os.makedirs(os.path.dirname(package_path), exist_ok=True)

    if args.dry_run:
        print("--dry-run option given, no package will be created")

    if not os.path.exists(metadata_path):
        abort('Path does not exist: {}'.format(metadata_path))

    format_ = os.path.splitext(metadata_path)[-1][1:]

    if format_ not in SUPPORTED_FORMATS:
        abort(
            'Input file must be in one of the following formats:\n * %s'
            % '\n * '.join(SUPPORTED_FORMATS))

    package = getattr(MediaCenterPackage, 'from_%s' % format_)(metadata_path)

    if not package.medias:
        abort("There is no (valid) media to create the package.\n"
              "No package will be created")

    elif not args.dry_run:
        package.create_package_zip(package_path)

    print("\nPackage {package_path} {verb} been created with"
          " {nb_media} medias."
          .format(
              verb="would have" if args.dry_run else "has",
              package_path=package_path,
              nb_media=len(package.medias)))

    if not args.dry_run:
        print("\nYaml to use is: \n")
        metadata = {
            k: getattr(args, k)
            for k in ('package_id', 'name', 'description', 'language', 'url')
        }
        yaml_metadata = generate_catalog_yaml_metadata(package_path, metadata)
        print(yaml.safe_dump(yaml_metadata, default_flow_style=False))


def main():
    args = get_parser().parse_args()
    handle(args)
    sys.exit(0)
